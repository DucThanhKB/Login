package com.ducthanh.loginimplementmvp.loginActivity.loginActivity;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.Toast;

import com.ducthanh.loginimplementmvp.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements LoginInterface.ViewInterface {

    private String mId;
    private String mPass;
    @BindView(R.id.text_input_layout_id)
    public TextInputLayout textInputLayoutId;
    @BindView(R.id.text_input_layout_password)
    public TextInputLayout textInputLayoutPass;
    @BindView(R.id.button_sign_in)
    public Button buttonSignIn;

    private LoginPresenter mLoginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mLoginPresenter = new LoginPresenter(this,this);
    }

    @OnClick(R.id.button_sign_in)
    public void signInPressed() {
        mId = textInputLayoutId.getEditText().getText().toString();
        mPass = textInputLayoutPass.getEditText().getText().toString();
        handleLogin();
    }

    private void handleLogin() {
        mLoginPresenter.checkSharePreferences();
        mLoginPresenter.checkLogin(mId, mPass);
    }

    @Override
    public void showToast(Boolean mCheckAccount) {
        if (mCheckAccount) {
            Toast.makeText(this, "Đăng nhập thành công!", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "ID hoặc mật khẩu không đúng!", Toast.LENGTH_LONG).show();
        }
    }
}
