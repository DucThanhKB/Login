package com.ducthanh.loginimplementmvp.loginActivity.loginActivity;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by DucThanh on 7/13/2017.
 */

public class LoginModel {
    private LoginInterface.PresenterInterface mPresenterInterface;
    SharedPreferences sPref;

    LoginModel(Context context, LoginInterface.PresenterInterface mPresenterInterface) {
        this.mPresenterInterface = mPresenterInterface;
        sPref = context.getSharedPreferences("Accounts", Context.MODE_PRIVATE);
    }

    public void putAccounts() {
        SharedPreferences.Editor editor = sPref.edit();
        editor.putString("id", "ducthanh");
        editor.putString("password", "ducthanh");
        editor.apply();
    }

    public String getId() {
        return sPref.getString("id", "missing");
    }

    public String getPassword() {
        return sPref.getString("password", "missing");
    }
}
