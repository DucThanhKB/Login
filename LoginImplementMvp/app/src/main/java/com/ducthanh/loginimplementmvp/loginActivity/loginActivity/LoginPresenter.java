package com.ducthanh.loginimplementmvp.loginActivity.loginActivity;

import android.content.Context;


/**
 * Created by DucThanh on 7/13/2017.
 */

public class LoginPresenter implements LoginInterface.PresenterInterface {
    LoginModel mModel;
    LoginInterface.ViewInterface mView;

    LoginPresenter(Context context, LoginInterface.ViewInterface mView) {
        this.mView = mView;
        this.mModel = new LoginModel(context, this);
    }

    public void checkSharePreferences() {
        mModel.putAccounts();
    }

    public void checkLogin(String id, String pass) {
        Boolean mCheckAccount;
        if (mModel.getId().equals(id) && mModel.getPassword().equals(pass)) {
            mCheckAccount = true;
        } else {
            mCheckAccount = false;
        }
        handleSignIn(mCheckAccount);
    }

    public void handleSignIn(Boolean mCheckAccount) {
        mView.showToast(mCheckAccount);
    }
}
