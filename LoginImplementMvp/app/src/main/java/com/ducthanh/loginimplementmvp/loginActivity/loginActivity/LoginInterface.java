package com.ducthanh.loginimplementmvp.loginActivity.loginActivity;

/**
 * Created by DucThanh on 7/13/2017.
 */

public interface LoginInterface {
    interface ViewInterface {
        void showToast(Boolean mCheckAccount);
    }
    interface PresenterInterface {

    }
}
